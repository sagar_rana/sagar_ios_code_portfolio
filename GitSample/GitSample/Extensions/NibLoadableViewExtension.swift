//
//  NibLoadableViewExtension.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit

extension NibLoadableView where Self: UIView {
    
    static var NibName: String {
        return String(describing: self)
    }
}
