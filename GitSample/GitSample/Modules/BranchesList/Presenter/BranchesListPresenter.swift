//
//  BranchesListPresenter.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit

class BranchesListPresenter: BranchesListPresenterProtocol {
    
    weak private var view: BranchesListViewProtocol?
    var interactor: BranchesListInteractorInputProtocol!
    private let router: BranchesListWireframeProtocol

    init(interface: BranchesListViewProtocol, interactor: BranchesListInteractorInputProtocol?, router: BranchesListWireframeProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }
    
    func fetchBranches() {
         self.view?.showActivityIndicator()
        self.interactor.retrieveBranchList()
    }
    
    func getBranchCount() -> Int {
        return self.interactor.getBranchList().count
    }
    
    func getBranch(atIndex: Int) -> BranchModel {
        return self.interactor.getBranchList()[atIndex]
    }

}
extension BranchesListPresenter:BranchesListInteractorOutputProtocol {
    func onBranchRetrieved() {
         self.view?.hideActivityIndicator()
        self.view?.onBranchesRetrieved()
    }
    
    func onError(error: NetworkErrors?) {
         self.view?.hideActivityIndicator()
        self.view?.showAlert(title: ErrorAlerts.Default.DataFetch.title , message: error?.description)
    }
    
    
}
