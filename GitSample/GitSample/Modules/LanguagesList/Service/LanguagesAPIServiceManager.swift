//
//  LanguagesAPIServiceManager.swift
//  GitSample
//
//  Created by hs on 13/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation

import Alamofire
import AlamofireObjectMapper

class LanguagesAPIServiceManager:BaseAPIService {
    
    
    
    func retrieveLanguagesList(userPath:String?,apiResponseHandler: @escaping(LanguageModel?, NetworkErrors?)->Void) -> Void  {
       
        guard let urlString = self.apiConfig?.url(scope: Endpoints.Languages.scope.path, userPath: userPath, module: Endpoints.Languages.fetch.path) else {
            return
        }
        let httpHeaders = self.apiConfig?.header()
        Alamofire
            .request(urlString, method: .get, headers:httpHeaders)
            .validate()
            .responseObject(completionHandler: { (response: DataResponse<LanguageModel>) in
                self.debugHTTPHeader(request: response.request! as NSURLRequest)
                switch response.result {
                case .success(let Languages):
                    
                    apiResponseHandler(Languages,nil)
                    
                case .failure(let error):
                    let errorInfo = self.error(withError: error, withData: response.data, responseCode: (response.response?.statusCode))
                    apiResponseHandler(nil,errorInfo)
                    
                }
            })
            
        
    }
    
    
    
    
}
