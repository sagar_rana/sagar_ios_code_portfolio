//
//  LanguagesListPresenter.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit

class LanguagesListPresenter: LanguagesListPresenterProtocol {

    weak private var view: LanguagesListViewProtocol?
    var interactor: LanguagesListInteractorInputProtocol!
    private let router: LanguagesListWireframeProtocol

    init(interface: LanguagesListViewProtocol, interactor: LanguagesListInteractorInputProtocol?, router: LanguagesListWireframeProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }
    func fetchLanguages() {
        self.view?.showActivityIndicator()
        self.interactor?.retrieveLanguageList()
    }
    func getLanguageList() -> [String] {
      return self.interactor.getLanguagesList()
    }
    func getlanguageCount() -> Int {
        
        return self.getLanguageList().count
    }
    func getlanguage(atIndex:Int) -> String? {
       return self.getLanguageList()[atIndex]
    }
}
extension LanguagesListPresenter : LanguagesListInteractorOutputProtocol {
    func onLanguageRetrieved() {
         self.view?.hideActivityIndicator()
        self.view?.onLanguageRetrieved()
    }
    
    func onError(error: NetworkErrors?) {
         self.view?.hideActivityIndicator()
     self.view?.showAlert(title: ErrorAlerts.Default.DataFetch.title , message: error?.description)
    }
    
    
}
