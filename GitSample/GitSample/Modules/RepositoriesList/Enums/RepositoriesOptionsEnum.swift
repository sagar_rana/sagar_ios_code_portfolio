//
//  RepositoriesOptionsEnum.swift
//  GitSample
//
//  Created by hs on 13/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation

protocol Option {
    var title: String { get }
}

enum RepositoriesOption:Option {
    case Launguage
    case Branch
    case Contributor
    public var title: String {
        switch self {
        case .Launguage: return "Launguages"
        case .Branch: return "Branches"
        case .Contributor: return "Contributors"
        }
    }
  
    
}
