//
//  RepositoriesListInteractor.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//


import UIKit

class RepositoriesListInteractor: RepositoriesListInteractorInputProtocol {
    
    var remoteService = RepositoriesAPIServiceManager()
    weak var presenter: RepositoriesListInteractorOutputProtocol?
    var repositoriesArray: [RepositoriesModel]?
    func retrieveRepositoriesList() {
        remoteService.retrieveRepositoriesList(){[weak self] (data, error) in
            guard let strongSelf = self else { return }

            if let repositoriesArray = data {

                strongSelf.repositoriesArray = repositoriesArray
                strongSelf.presenter?.onRepositoriesRetrieved()

            }
            else {
                strongSelf.presenter?.onError(error: error)

            }
            
        }
    }
    func getRepositoriesList() -> [RepositoriesModel] {
        var repositoriesArray = [RepositoriesModel]()
        if let data = self.repositoriesArray {
            repositoriesArray = data
        }
        return repositoriesArray
    }

}

