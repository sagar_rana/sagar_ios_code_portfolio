//
//  RepositoriesOptionInteractor.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit

class RepositoriesOptionInteractor: RepositoriesOptionInteractorInputProtocol {

    weak var presenter: RepositoriesOptionInteractorOutputProtocol?
    var data:RepositoriesModel?
    func fetchData(){
        if self.data != nil {
         self.presenter?.onDataAvailable()
        }
        else{
         self.presenter?.onDataNotAvailable()
        }
    }
    func getRepositoriesData() -> RepositoriesModel?{
        return self.data
    }
}
