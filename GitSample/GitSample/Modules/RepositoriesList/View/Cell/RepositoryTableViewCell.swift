//
//  RepositoryTableViewCell.swift
//  GitSample
//
//  Created by hs on 13/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit
import Material
import Motion
class RepositoryTableViewCell: TableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblTitle.text = nil
        // Initialization code
        self.prepareTitleLable()
    }
    func prepareTitleLable(){
        lblTitle.font = RobotoFont.regular(with: 16)
        
    }

    open override func prepare() {
        super.prepare()

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
    func setTitle(title:String?) {
        if let title = title {
            self.lblTitle?.text = title.firstUppercased
        }
    }
}
