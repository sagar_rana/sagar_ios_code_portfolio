//
//  RepositoriesListViewController.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//


import UIKit
import Material
class RepositoriesListViewController: BaseViewController {

	var presenter: RepositoriesListPresenterProtocol!

    @IBOutlet weak var tableView: TableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpViewComponents()
        self.presenter.fetchRepositories()
    }
    class var className: String {
        return String(describing: self)
    }
    func setUpViewComponents(){
        self.prepareNavigationItem()
        self.prepareTableView()
    }
 
   
}
extension RepositoriesListViewController:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.getRepositoriesCount()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        
        let cell = tableView.dequeueReusableCell(withIdentifier: RepositoryTableViewCell.reuseIdentifier, for: indexPath) as! RepositoryTableViewCell
        let data  = self.presenter.getRepositoriesData(atIndex: indexPath.row)
        cell.setTitle(title:data.name)
       return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
      self.presenter.showOptions(atIndex:indexPath.row)
    }
}


extension RepositoriesListViewController {
    func prepareNavigationItem(){
        self.setnavigationTitle(title: Screens.Repositories.RepositoriesList.title)
    }
    func prepareTableView(){
        tableView.estimatedRowHeight = ViewSizes.TableViewCell.SingleLineData.estimatedRowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
        tableView.backgroundColor = nil
        self.registerNib()
        
    }
    func registerNib() {
        tableView.register(RepositoryTableViewCell.self)
    }
}

extension RepositoriesListViewController:RepositoriesListViewProtocol {
    func onRepositoriesRetrieved() {
        self.setnavigationTitle(title: "\(Screens.Repositories.RepositoriesList.title):\(self.presenter.getRepositoriesCount())")
        
        self.tableView.reloadData()
    }
    
  
}
