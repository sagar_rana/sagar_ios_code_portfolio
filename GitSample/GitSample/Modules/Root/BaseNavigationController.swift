//
//  BaseNavigationController.swift
//  GitSample
//
//  Created by hs on 12/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import UIKit
import Material

class BaseNavigationController: NavigationController {
    open override func prepare() {
        super.prepare()
        isMotionEnabled = false
        
        guard let v = navigationBar as? NavigationBar else {
            return
        }
        
        v.depthPreset = .none
        v.dividerColor = Color.grey.lighten2
    }
}
