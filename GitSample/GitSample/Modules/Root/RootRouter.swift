//
//  RootRouter.swift
//  GitSample
//
//  Created by Sagar Rana on 12/05/18.
//  Copyright © 2018 hs. All rights reserved.
//

import UIKit
class RootRouter: RootWireframe {
    
    func presentRepositoriesScreen(in window: UIWindow) {
        window.rootViewController = BaseNavigationController(rootViewController: RepositoriesListRouter.createModule())
        window.makeKeyAndVisible()


    }
}
