//
//  ErrorAlertEnum.swift
//  GitSample
//
//  Created by hs on 14/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation
protocol ErrorAlert {
    var title: String { get }
    var message: String { get }
    
}

enum ErrorAlerts {
    
    enum Default:ErrorAlert {
           case Network
           case DataFetch
        
        public var title: String {
            switch self {
            case .Network: return "error!"
            case .DataFetch:
                return "error!"
            }
        }
        
        public var message: String {
            switch self {
            case .Network: return "Internet not available. Please try again"
            case .DataFetch: return  "problem in getting data."
            }
        }
        
    }
}
