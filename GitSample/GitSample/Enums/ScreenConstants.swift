//
//  ScreenConstants.swift
//  GitSample
//
//  Created by hs on 13/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation
protocol Screen {
    var title: String { get }
}

enum Screens {
    
    enum Repositories: Screen {
        case RepositoriesList
        case RepositoriesOption

        public var title: String {
        
            switch self {
            case .RepositoriesList: return "Repositories"
            case .RepositoriesOption: return "Option"
            }
        }
    }
    enum Contributions: Screen {
        case ContributionsList
        public var title: String {
            switch self {
            case .ContributionsList: return "Contributions"
            }
        }
    }
    enum Languages: Screen {
        case LanguagesList
        public var title: String {
            switch self {
            case .LanguagesList: return "Languages"
            }
        }
    }
    enum Branches: Screen {
        case BranchesList
        public var title: String {
            switch self {
            case .BranchesList: return "Branches"
            }
        }
    }
   
}
