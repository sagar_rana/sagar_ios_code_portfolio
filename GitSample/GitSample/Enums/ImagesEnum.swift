//
//  ImagesEnum.swift
//  GitSample
//
//  Created by hs on 13/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation
import UIKit
protocol PlaceHolder {
    var image: UIImage { get }
    var name: String { get }
    
}

enum PlaceHolders {
    
    enum Default:PlaceHolder {
        case Avtar
        public var image: UIImage {
            switch self {
            case .Avtar: return UIImage.init(named: "AvtarPlaceHolder")!
            }
        }
        public var name: String {
            switch self {
            case .Avtar: return "AvtarPlaceHolder"
            }
        }
    }
}

