//
//  LanguageModel.swift
//  GitSample
//
//  Created by hs on 13/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation

import ObjectMapper

struct LanguageModel {
    var languageName :[String]?
    
}

extension LanguageModel: Mappable {
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        
         self.languageName = map.JSON.map { (key,value)  in return key.capitalized }
        
        
    }
}
