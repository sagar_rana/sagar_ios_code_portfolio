//
//  ContributorModel.swift
//  GitSample
//
//  Created by hs on 13/05/18.
//  Copyright © 2018 Sagar Rana. All rights reserved.
//

import Foundation
import ObjectMapper

struct ContributorModel {
    var login: String?
    var avatarUrl: String?
}

extension ContributorModel: Mappable {
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        login   <- map["login"]
        avatarUrl     <- map["avatar_url"]
    }
}
