//
//  Endpoints.swift
//  GitSample
//
//  Created by Sagar Rana on 12/05/18.
//  Copyright © 2018 hs. All rights reserved.
//

import Foundation

struct API {

    static let scheme = "https"
    static let host = "api.github.com"
    static let baseUrl = "https://api.github.com"
    
}

protocol Endpoint {
    var path: String { get }
}

enum Endpoints {
    
    enum Repositories: Endpoint {
        case fetch
        
        public var path: String {
            switch self {
            case .fetch: return "/repositories"
            }
        }
        
        public var url: String {
            switch self {
            case .fetch: return "\(API.baseUrl)\(path)"
            }
        }
        public var params: [String:Any] {
            switch self {
            case .fetch: return ["since":0]
            }
        }
    }
    enum Languages: Endpoint {

        case scope
        case fetch

        public var path: String {
            switch self {
            case .scope: return "/repos"
            case .fetch: return "/languages"
            }
        }
    }
    enum contributors: Endpoint {
        
        case scope
        case fetch
        
        public var path: String {
            switch self {
            case .scope: return "/repos"
            case .fetch: return "/contributors"
            }
        }
    }
    
    enum branches: Endpoint {
        
        case scope
        case fetch
        
        public var path: String {
            switch self {
            case .scope: return "/repos"
            case .fetch: return "/branches"
            }
        }
    }
   
}
